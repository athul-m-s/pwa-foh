import { HttpClient } from '@angular/common/http';
import { ApplicationRef, Component, OnInit } from '@angular/core';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';
import { IndexedDBService } from './indexed-db.service.ts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'pwa-foh';
  test: any = [];
  constructor(private http: HttpClient, private update: SwUpdate, private appRef: ApplicationRef, private swPush: SwPush, private indexedDBService: IndexedDBService) {
    this.updateClient();
    this.checkUpdate();
  }

  ngOnInit(): void {
    this.pushSubscription();
    this.http.get('	https://cat-fact.herokuapp.com/facts').subscribe((res: any) => {
      this.test = res;
    })


    //here we will get the notification from server
    this.swPush.notificationClicks.subscribe(({ action, notification }) => {
      window.open(notification.data.url);
    });

    addEventListener('offline', (e) => {

    })

    addEventListener('online', (e) => {

    })

    if (!navigator.onLine) {

    }

    if (navigator.onLine) {

    }
  }

  updateClient() {
    if (!this.update.isEnabled) {
      console.log('Not Enabled');
      return;
    }
    this.update.available.subscribe((event) => {
      console.log(`current`, event.current, `available `, event.available);
      if (confirm('update available for the app please conform')) {
        this.update.activateUpdate().then(() => {
          location.reload();
          //if server api call can also trigger here
        });
      }
    });

    this.update.activated.subscribe((event) => {
      console.log(`current`, event.previous, `available `, event.current);
    });

    this.update.checkForUpdate().then((res) => {
      console.log("this will be true or false " + res);
    })
  }

  checkUpdate() {
    this.appRef.isStable.subscribe((isStable) => {
      if (isStable) {
        //in 8hr it will check for update and if update exits then it will trigger the update.available subscription
        const timeInterval = interval(8 * 60 * 60 * 1000);

        timeInterval.subscribe(() => {
          this.update.checkForUpdate().then(() => console.log('checked'));
          console.log('update checked');
        });
      }
    });
  }

  publicKey = ""; //should get from server code

  //this function should be called in a btn click or ngOninit
  pushSubscription() {
    if (!this.swPush.isEnabled) {
      console.log('Notification is not enabled');
      return;
    }

    this.swPush
      .requestSubscription({
        serverPublicKey: this.publicKey,
      })
      .then((sub) => {
        // Make a post call to serve
        console.log(JSON.stringify(sub));
        //this will create an object that object need to be added in service 
      })
      .catch((err) => console.log(err));
  }

  //this will call on btn click
  postCall() {
    let obj = {
      name: 'Subrat',
    };
    //api call
    this.http.post('http://localhost:3000/data', obj).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => {
        //if fails then register
        // this.backgroundSync();

        this.indexedDBService
          .addUser(obj.name)
          .then(this.backgroundSync)
          .catch(console.log);
      }
    );
  }

  backgroundSync() {
    navigator.serviceWorker.ready
      .then((swRegistration: any) => swRegistration.sync.register('post-data'))
      .catch(console.log);
  }
}
