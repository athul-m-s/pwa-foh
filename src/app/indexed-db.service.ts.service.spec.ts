import { TestBed } from '@angular/core/testing';

import { IndexedDbServiceTsService } from './indexed-db.service.ts.service';

describe('IndexedDbServiceTsService', () => {
  let service: IndexedDbServiceTsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IndexedDbServiceTsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
